/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
Platform,
StyleSheet,
Text,
View,
Switch,
Image,
PixelRatio,
KeyboardAvoidingView,
ScrollView,
TextInput,
TouchableHighlight,
TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination,ParallaxImage } from 'react-native-snap-carousel';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { NavigationActions } from 'react-navigation';
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'

import SplashScreen from 'react-native-smart-splash-screen'

import styles from './styles.js'
import { ENTRIES1 } from './entries';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


export default class UpcomingWorkouts9 extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0,
      slider1Ref :null,
      slider1ActiveSlide: 1,
    };
  }

  _renderWorkout= ({item, index},parallaxProps)=> {

    return (
        <View >
          <TouchableOpacity
              activeOpacity={1}
              style={styles.carousel_style_workout}
              onPress={() => { alert('hi'); }}
              >
                <View style={styles.carousel_image_view}>

                    <ParallaxImage
                    source={{ uri: item.illustration }}
                    containerStyle={{flex: 1,backgroundColor: 'white',}}
                    style={styles.carousel_image}
                    parallaxFactor={0.35}
                    showSpinner={true}
                    spinnerColor={'rgba(0, 0, 0, 0.25)'}
                    {...parallaxProps}
            />

                </View>
                <View style={styles.carousel_text_view}>
                    <Text
                      style={styles.carousel_text}
                      numberOfLines={2}
                    >
                      some text
                    </Text>

                </View>
            </TouchableOpacity>
        </View>
    );
}


    _renderTrainers= ({item, index},parallaxProps)=> {

      return (
          <View >
            <TouchableOpacity
                activeOpacity={1}
                style={styles.carousel_style_trainers}
                onPress={() => { alert('hi'); }}
                >
                  <View style={styles.carousel_image_view}>

                      <ParallaxImage
                      source={{ uri: item.illustration }}
                      containerStyle={{flex: 1,backgroundColor: 'white',}}
                      style={styles.carousel_image}
                      parallaxFactor={0.35}
                      showSpinner={true}
                      spinnerColor={'rgba(0, 0, 0, 0.25)'}
                      {...parallaxProps}
              />

                  </View>
                  <View style={styles.carousel_text_view}>
                      <Text
                        style={styles.carousel_text}
                        numberOfLines={2}
                      >
                      some text
                      </Text>

                  </View>
              </TouchableOpacity>
          </View>
      );
  }


      _renderPlans= ({item, index},parallaxProps)=> {

        return (
            <View >
              <TouchableOpacity
                  activeOpacity={1}
                  style={styles.carousel_style_plans}
                  onPress={() => { alert('hi'); }}
                  >
                    <View style={styles.carousel_image_view}>

                        <ParallaxImage
                        source={{ uri: item.illustration }}
                        containerStyle={{flex: 1,backgroundColor: 'white',}}
                        style={styles.carousel_image}
                        parallaxFactor={0.35}
                        showSpinner={true}
                        spinnerColor={'rgba(0, 0, 0, 0.25)'}
                        {...parallaxProps}
                />

                    </View>
                    <View style={styles.carousel_text_view}>
                        <Text
                          style={styles.carousel_text}
                          numberOfLines={2}
                        >
                          some text
                        </Text>

                    </View>
                </TouchableOpacity>
            </View>
        );
    }


render() {
  const {navigation} = this.props
  const resizeMode = 'center';
  const text = 'I am some centered text';
  return (

    <View style={styles.mainBody} >


          <ScrollView
            style={{flex:1}}
            contentContainerStyle={{paddingBottom: 50}}
            indicatorStyle={'white'}
            scrollEventThrottle={200}
            directionalLockEnabled={true}
          >
            <View style={styles.listofWrkouts1}>
              <View style={styles.chevron_left_icon}>
                <TouchableOpacity onPress={()=>this.onPressChevronLeft(navigation )}>
                    <FontAwesome name="chevron-left" size={25} color="#FF7E00"   />
                </TouchableOpacity>
              </View>

              <View style={styles.header}>
                    <Text style={styles.topSignupTxt}>
                      Activity
                    </Text>
              </View>

              <View>
                <Text style = {styles.text_workout_heading}>
                  Log your activity
                </Text>
                <Text style = {styles.text_workout_sub_heading}>
                  ADO YOUR SPORT. TIME & RATE TO TRACK YOUR RESULTS
                </Text>
              </View>


            </View>

            <View style={styles.feed_img_mas2a}>
              <Text style={styles.sport1}>SPORT :</Text>
              <Text style={styles.log_act1}>Lindsey Baah</Text>
            </View>

            <View style={styles.feed_img_mas2a}>
              <Text style={styles.sport1}>TIME :</Text>
              <TextInput  placeholder="00:00:00" underlineColorAndroid='transparent' autoCorrect={false} placeholderTextColor='#626264' style={styles.textInput_login} />

            </View>

            <View style={styles.feed_img_mas2a}>
              <Text style={styles.sport1}>DATE :</Text>
              <TextInput  placeholder="DD/MM/YYY" underlineColorAndroid='transparent' autoCorrect={false} placeholderTextColor='#626264' style={styles.textInput_login} />
            </View>


          </ScrollView>




         



          </View>

  );
}
}
