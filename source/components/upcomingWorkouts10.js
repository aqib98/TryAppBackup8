/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
Platform,
StyleSheet,
Text,
View,
Switch,
Image,
PixelRatio,
KeyboardAvoidingView,
ScrollView,
TouchableHighlight,
TouchableOpacity,
ToastAndroid
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination,ParallaxImage } from 'react-native-snap-carousel';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import { ProgressCircle }  from 'react-native-svg-charts'

import PropTypes from 'prop-types';
import { requireNativeComponent, NativeModules,  ViewPropTypes} from 'react-native';

import SplashScreen from 'react-native-smart-splash-screen'

import Video from 'react-native-video'

import styles from './styles.js'
import { ENTRIES1 } from './entries';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


export default class UpcomingWorkouts10 extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0,
      slider1Ref :null,
      slider1ActiveSlide: 1,
    };
  }
  video: Video;

    _playpause=()=>{
      this.setState({ paused: !this.state.paused })
    }

    onLoad = (data) => {
      this.setState({ duration: data.duration });
      ToastAndroid.show('Loading Video Please Wait...' , ToastAndroid.SHORT);
    };

    _video(){
      this.state.min = Math.floor(data.duration / 60)+':'+Math.floor(data.duration % 60);
    }

    onProgress = (data) => {

      this.setState({ currentTime: data.currentTime });

      this.state.seconds = this.state.duration - this.state.currentTime

      console.log(this.state.seconds)

      var minutes = Math.floor(this.state.seconds / 60);

      var second = this.state.seconds - minutes * 60;

      this.setState({ timevalue : minutes +':' + Math.floor(second)})


    };

    onEnd = () => {
      this.setState({ paused: true })
      this.video.seek(0)
    };

    onAudioBecomingNoisy = () => {
      this.setState({ paused: true })
    };

    onAudioFocusChanged = (event: { hasAudioFocus: boolean }) => {
      this.setState({ paused: !event.hasAudioFocus })
    };

    getCurrentTimePercentage() {
      if (this.state.currentTime > 0) {
        return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
      }
      return 0;
    };

    renderRateControl(rate) {
      const isSelected = (this.state.rate === rate);

      return (
        <TouchableOpacity onPress={() => { this.setState({ rate }) }}>
          <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
            {rate}x
          </Text>
        </TouchableOpacity>
      );
    }

    renderResizeModeControl(resizeMode) {
      const isSelected = (this.state.resizeMode === resizeMode);

      return (
        <TouchableOpacity onPress={() => { this.setState({ resizeMode }) }}>
          <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
            {resizeMode}
          </Text>
        </TouchableOpacity>
      )
    }

    renderVolumeControl(volume) {
      const isSelected = (this.state.volume === volume);

      return (
        <TouchableOpacity onPress={() => { this.setState({ volume }) }}>
          <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
            {volume * 100}%
          </Text>
        </TouchableOpacity>
      )
    }

render() {
  const {navigation} = this.props
  const resizeMode = 'center';
  const text = 'I am some centered text';
  return (

    <View style={styles.mainBody1}>

    <View style={styles.containervv}>

    <View>

    </View>
     <TouchableOpacity
       style={styles.fullScreen1vv}
       onPress={() => this._playpause(this.state.play)}
     >
       <Video
         ref={(ref: Video) => { this.video = ref }}
         /* For ExoPlayer */
         /* source={{ uri: 'http://www.youtube.com/api/manifest/dash/id/bf5bb2419360daf1/source/youtube?as=fmp4_audio_clear,fmp4_sd_hd_clear&sparams=ip,ipbits,expire,source,id,as&ip=0.0.0.0&ipbits=0&expire=19000000000&signature=51AF5F39AB0CEC3E5497CD9C900EBFEAECCCB5C7.8506521BFC350652163895D4C26DEE124209AA9E&key=ik0', type: 'mpd' }} */
         source={{uri: 'http://mediaservicecheck.streaming.mediaservices.windows.net/04675a84-a460-4c62-8472-872635836595/Standing%20calf%20raise_960x540_2250.mp4'}}
         style={styles.fullScreenvv}
         rate={this.state.rate}
         paused={this.state.paused}
         volume={this.state.volume}
         muted={this.state.muted}
         resizeMode="cover"
         onLoad={this.onLoad}
         onProgress={this.onProgress}
         onEnd={this.onEnd}
         onLoadStart={this.loadStart}
         onAudioBecomingNoisy={this.onAudioBecomingNoisy}
         onAudioFocusChanged={this.onAudioFocusChanged}
       />
     </TouchableOpacity>

     <View style={styles.controlsvv}>
       <View style={styles.generalControlsvv}>


         <View style={{position:'absolute', zIndex:999, height:175, width:125, right:20, top:80,left:10}}>
         <Text style={{textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: '#fff', position:'absolute', left:20, top:20}}>{this.state.heartrate}</Text>
         <ProgressCircle
                 style={ { height: 70, width:70, position:'relative',top:0,left:15, zIndex:999 } }
                 progress={ 0.7 }
                 progressColor={'rgb(217, 63, 106)'}
             />
             <View style={ { height: 135, width:105, position:'absolute',top:80,left:0, zIndex:999 } }>
             <Text style={ { color:'#fff', backgroundColor:'#ff7e00', borderRadius:8, padding:4 } }>HR Connected</Text>
             <Switch
                onValueChange = {()=>{
                  console.log('checked')
                }}
                value = {true} style={ {position:'absolute', left:22, top:33} } />
                <Text style={ { color:'#fff', padding:4, marginTop:35, marginLeft:25 } }>Hide all</Text>
             </View>

         </View>





         <View style={{position:'absolute', zIndex:999, height:72, width:72, right:0, bottom:0}}>
          <Text style={{textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: '#fff', position:'absolute', left:20, top:20}}>{this.state.timevalue}</Text>
         <ProgressCircle
                 style={ { height: 70, width:70, position:'relative',top:0,right:0, zIndex:999 } }
                 progress={ 0.7 }
                 progressColor={'rgb(217, 63, 106)'}
             />

         </View>

       </View>
       </View>


     </View>
    </View>

  );
}
}
