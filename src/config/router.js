import React from "react";
import { Platform, StatusBar, HeaderRight, StyleSheet, Button, Image } from "react-native";
import { StackNavigator, TabNavigator } from "react-navigation";

import Login from "../components/screens/login";
import Signup from "../components/screens/signup";
import Courses from "../components/screens/courses";
import SearchDevices from "../components/screens/searchdevices";
import WorkoutCalender from "../components/screens/workoutcalender";
import StartExcierce from "../components/screens/startexcierce";
import CourseData from "../components/screens/coursedata";
import TrackExcierce from "../components/screens/trackexcierce";
import FinishExcierce from "../components/screens/finishexcierce";
import LogActivity from "../components/screens/logactivity";
import VideoExcierce from "../components/screens/videoexcierce";
import Clock from "../components/screens/clock";


const styles = StyleSheet.create({
  container: {
   flex: 1,
   justifyContent: 'center',
  },
  list: {
    marginTop:10,
    margin: 1,
    borderColor: '#000',
    borderBottomWidth: 5,
    flex: 1
  },
  item: {
    fontSize: 20,
    paddingVertical: 5,
    color: '#000',
    borderColor: '#000',
    borderBottomWidth: 1,
    flex: 1
  },
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  avatar: {
    height: 35,
    width: 35,
    marginLeft: 15
  }
})

const headerStyle = {

};

export const SignedOut = StackNavigator({

  Login: {
    screen: Login,
    navigationOptions: {
      title: "Log In",
      header: null,
    }
  },
  Signup: {
    screen: Signup,
    navigationOptions: {
      title: "Sign Up",
      headerStyle
    }
  }
});



export const SignedIn = StackNavigator({


    Clock: {
        screen: Clock,
        navigationOptions: {
            title: "Courses",
            header: null
        }
    },
    StartExcierce: {
        screen: StartExcierce,
        navigationOptions: {
            title: "Courses",
            header: null
        }
    },    
    TrackExcierce: {
        screen: TrackExcierce,
        navigationOptions: {
            title: "Courses",
            header: null
        }
    },
    LogActivity: {
        screen: LogActivity,
        navigationOptions: {
            title: "Courses",
            header: null
        }
    },
    Courses: {
        screen: Courses,
        navigationOptions: {
            title: "Courses",
            header: null
        }      
    },
    VideoExcierce: {
        screen: VideoExcierce,
        navigationOptions: {
            title: "Courses",
            header: null
        }       
    }



/*    Courses: {
        screen: Courses,
        navigationOptions: {
            title: "Courses",
            header: null
        }
    },
    Workouts: {
        screen: Workouts,
        navigationOptions: ({navigation}) => ({
          title: `${navigation.state.params.courseTitle}`,
          header: null,
        }),
    },
    Exercises: {
        screen: Exercises,
        navigationOptions: ({navigation}) => ({
          title: `${navigation.state.params.exeTitle}`,
          header: null
        }),
    },
    ExerciseDetails: {
      screen: ExerciseDetails,
        navigationOptions: ({navigation}) => ({
          title: `${navigation.state.params.exeTitle}`,
          header: null
        }),
    },
    Menu: {
      screen: Menu,
      navigationOptions: {
        title: "Menu",
        header: null
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        title: "Profile",
        header: null,
      }
    },
    BluetoothIntegrate: {
      screen: BluetoothIntegrate,
      navigationOptions: {
        title: "BluetoothIntegrate",
        header: null,
      }
    }, 
    Heartrate: {
      screen: Heartrate,
      navigationOptions: {
        title: "Heartrate",
        header: null,
      }
    },
    Instructer: {
      screen: Instructer,
      navigationOptions: {
        title: "Instructer ",
        header: null,
      }
    }           

*/

});




export const createRootNavigator = (signedIn = false) => {
  return StackNavigator(
    {
      SignedIn: {
        screen: SignedIn,
        navigationOptions: {
          gesturesEnabled: false
        }
      },
      SignedOut: {
        screen: SignedOut,
        navigationOptions: {
          gesturesEnabled: false
        }
      }
    },
    {
      headerMode: "none",
      mode: "modal",
      initialRouteName: signedIn ? "SignedIn" : "SignedOut"
    }
  );
};
