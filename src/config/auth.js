import { AsyncStorage } from "react-native";

async function onSignIn(username, id){
  console.log(username, id)
  AsyncStorage.setItem("USER_NAME", String(username));
  AsyncStorage.setItem("USER_ID", String(id));
  let USER_ID = await AsyncStorage.getItem("USER_ID"); 
  let USER_NAME = await AsyncStorage.getItem("USER_NAME"); 
  console.log(USER_ID, USER_NAME)
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("USER_ID")
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
}

export const addAsyncStorage = (data) => {
  return new Promise((resolve, reject)=>{
    var i = 0;
    Object.keys(data).map(key=>{
      console.log(key, data[key])
      AsyncStorage.setItem(key, data[key])
      .then(res => {
          i++;
      })
      .catch(err => reject({'err':err,'message':'error in storing'}));
    })
    console.log(Object.keys(data).length, i)
    resolve({
      'message':'Successfully stored',
    })
  });
}
export const getAsyncStroage = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("USER_ID")
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
}
export const onSignOut = () => {
  AsyncStorage.removeItem("USER_NAME");
  AsyncStorage.removeItem("USER_ID");
}
export const isSignedIn = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("USER_NAME")
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};
export const getStudentId = async ()=> { 
    let USER_ID = await AsyncStorage.getItem("USER_ID"); 
    return(USER_ID);   
}  

export const getStudentName = () =>{
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("USER_NAME")
      .then(res => {
        if (res !== null) {
          resolve(res);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
}
export { onSignIn };