import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { onSignIn } from '../../config/auth';
import CustomTextField from '../template/textfield';
import Card from '../template/card';
import CardSection from '../template/cardSection';
import CardDetails from '../template/cardDetails';
import Button from '../template/button';

export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      Password: ""
    };
  }
  componentWillMount(){
    console.log(this.props.navigation);
  }
  toSignup () {

    const { navigate } = this.props.navigation;
    navigate('Signup');
  }

  render() {
    const { navigate } = this.props.navigation;
    const 
    { 
      loginContainerStyle,
      textFieldIconContainerStyle,
      iconStyle,
      textFieldContainerStyle,
      fpTextStyle,
      signupTextStyle
    } = styles;
    return (
      <View style = {loginContainerStyle}>
        <Card>
          <CardSection>
            <View style = {textFieldIconContainerStyle}>
              <Ionicons name='md-mail' style={iconStyle} />
            </View>
            <View style = {textFieldContainerStyle}>
              <CustomTextField 
              placeholder = 'Username'
              onChangeText={
                (e) => { this.setState({username:e}) }
              }
              />
            </View>
          </CardSection>
          <CardSection>
            <View style = {textFieldIconContainerStyle}>
              <Ionicons name='md-key' style={iconStyle} />
            </View>
            <View style = {textFieldContainerStyle}>
              <CustomTextField placeholder = 'Password'/>
            </View>
          </CardSection>
          <CardDetails>
            <Button 
              onPress={
                () => {
                  //onSignIn().then(() => navigate("SignedIn"));
                  alert(this.state.username)
                }
              }
            >
              <Text>SUBMIT</Text>
            </Button>
            <TouchableOpacity style = {fpTextStyle}
              onPress={
                () => alert("clicked")
              }
            >
              <Text style = {{color:'#4A4A4A'}}>Forgot Password?</Text>
            </TouchableOpacity>
          </CardDetails>
          <CardDetails>
            <TouchableOpacity style = {signupTextStyle} 
              onPress={ this.toSignup.bind(this) }
            >
              <Text style = {{color:'#4A4A4A'}}>Not a memeber? Signup</Text>
            </TouchableOpacity>
          </CardDetails>
        </Card>
      </View>
    );
  }
}

Login.propTypes = {
  navigation: PropTypes.object,
};

const styles = StyleSheet.create({
  loginContainerStyle: {
    marginTop: 40,
  },
  textFieldIconContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  iconStyle: {
    color: '#429DC6',
    fontSize: 30
  },
  textFieldContainerStyle: {
    flexDirection: 'column',
    flex: 1,
    width: null
  },
  fpTextStyle: {
    marginLeft: 10,
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  signupTextStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',

  },
  seprator: {
    borderBottomWidth: 1
  }
});
