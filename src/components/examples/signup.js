
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { onSignIn } from '../../config/auth';
import CustomTextField from '../template/textfield';
import Card from '../template/card';
import CardSection from '../template/cardSection';
import CardDetails from '../template/cardDetails';
import Button from '../template/button';

export default class Signup extends React.Component {

  constructor(props) {
    super(props);
  }
  componentWillMount(){
    console.log(this.props.navigation);
  }
  render() {

    const 
    { 
      signupContainerStyle,
      textFieldIconContainerStyle,
      iconStyle,
      textFieldContainerStyle,
      fpTextStyle,
      loginTextStyle
    } = styles;
    return (
      <View style = {signupContainerStyle}>
        <Card>
          <CardSection>
            <View style = {textFieldIconContainerStyle}>
              <Ionicons name='md-mail' style={iconStyle} />
            </View>
            <View style = {textFieldContainerStyle}>
              <CustomTextField placeholder = 'Username'/>
            </View>
          </CardSection>
          <CardSection>
            <View style = {textFieldIconContainerStyle}>
              <Ionicons name='md-key' style={iconStyle} />
            </View>
            <View style = {textFieldContainerStyle}>
              <CustomTextField placeholder = 'Password'/>
            </View>
          </CardSection>
          <CardSection>
            <View style = {textFieldIconContainerStyle}>
              <Ionicons name='md-key' style={iconStyle} />
            </View>
            <View style = {textFieldContainerStyle}>
              <CustomTextField placeholder = 'Confirm Password'/>
            </View>
          </CardSection>
          <CardDetails>
            <Button 
              onPress={
                () => {
                  onSignIn().then(() => navigation.navigate("SignedIn"));
                }
              } 
            >
              <Text>SUBMIT</Text>
            </Button>
            <TouchableOpacity style = {fpTextStyle}
              onPress={
                () => alert("clicked")
              }
            >
              <Text style = {{color:'#4A4A4A'}}>Forgot Password?</Text>
            </TouchableOpacity>
          </CardDetails>
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  signupContainerStyle: {
    marginTop: 40,
  },
  textFieldIconContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  iconStyle: {
    color: '#429DC6',
    fontSize: 30
  },
  textFieldContainerStyle: {
    flexDirection: 'column',
    flex: 1,
    width: null
  },
  fpTextStyle: {
    marginLeft: 10,
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  loginTextStyle: {
    alignItems: 'center',
    justifyContent: 'space-around',

  },
  seprator: {
    borderBottomWidth: 1
  }
});
