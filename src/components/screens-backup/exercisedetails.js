import React, { Component } from 'react';
import {
  AlertIOS,
  AppRegistry,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TouchableHighlight,
  Image,
  ScrollView,
  ToastAndroid
} from 'react-native';import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn } from '../../config/auth';
import { getExercises, getExerciseDetails, getSensorData, sendStatus } from '../template/api.js'
import { getProfileDetails, getSensorDataSQLite } from '../template/SQLiteOperationsOffline.js';

import Video from 'react-native-video';

export default class ExerciseDetails extends Component {

    constructor(props) {
    super(props);
    this.state={
      exercise:[],
      rate: 1,
      volume: 1,
      duration: 0.0,
      currentTime: 0.0,
      paused: true,
      loader:false,
      sensor_data:null,
      showSensorData:false
    }
  }
  video: Video;
  componentWillMount(){
    var { exerciseID, user_id, status, workout_day  } = this.props.navigation.state.params;
    //console.log(this.props.navigation.state.params)
    this._getExercise(exerciseID, status);
    this._getSensorData();
    //console.log(status)
  }
  onLoad = (data) => {
    this.setState({ duration: data.duration });
  };

    onProgress = (data) => {
      this.setState({ currentTime: data.currentTime });
    };

    onEnd = () => {
      this.setState({ paused: true })
      this.video.seek(0)
    };

    onAudioBecomingNoisy = () => {
      this.setState({ paused: true })
    };

    onAudioFocusChanged = (event: { hasAudioFocus: boolean }) => {
      this.setState({ paused: !event.hasAudioFocus })
    };
  _getExercise=(exerciseID, status)=>{
    console.log(status)
    getExerciseDetails(exerciseID).then(response=>{
      this.setState({exercise:response.data[0]});
      if(status.toLowerCase() === 'completed'){
        this.setState({showSensorData:true})
      }
      //console.log(response.data[0])
    },err=>{
      console.log(err)
    })
  }
  _playpause=()=>{
    this.setState({ paused: !this.state.paused })
  }
  getCurrentTimePercentage() {
      if (this.state.currentTime > 0) {
        return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
      }
      return 0;
    };

    _viewdevices = () => {
      var { user_id, exerciseID, exeTitle, status, workoutID, workoutTitle, courseTitle, courseID } = this.props.navigation.state.params;
      const { navigate  } = this.props.navigation;
        
        this.setState({loader:true})

      this._getStudentDetails(user_id);
    

    }

  _getStudentDetails=(id)=>{
    getProfileDetails(id).then(response=>{
      
      //console.log(response)
      if(response.status){
        this.setState({ height:response.data.height_in_feet, weight:response.data.current_weight, age:response.data.age});

        this.setState({loader:false})

      if(this.state.age == null || this.state.weight == null)
      {
        ToastAndroid.show('Please fill your height and weight details in Profile Page before starting Exercises', ToastAndroid.SHORT);
      }
      else{

      var { user_id, exerciseID, exeTitle, workout_day, status, workoutID, workoutTitle, courseTitle, courseID, course } = this.props.navigation.state.params;
      
      const { navigate  } = this.props.navigation;

      //this.setState({ paused: !this.state.paused })
        
      navigate("BluetoothIntegrate",{ exerciseID: exerciseID, workout_day: workout_day, exeTitle: exeTitle, user_id:user_id, status: status, workoutID: workoutID, workoutTitle: workoutTitle, courseID: courseID, courseTitle: courseTitle, videoUrl: this.state.exercise.mp4url, course: course, calories: this.state.calories });
      }
      
      }else{
        ToastAndroid.show(response.show, ToastAndroid.SHORT);
      }
    },err=>{
      console.log(err)
    })
  }


    _openMenu = async() => {

    const { navigate  } = this.props.navigation;
    var { workoutID, user_id, workoutTitle, courseTitle, courseID, course, workout_day } = this.props.navigation.state.params;

    console.log(this.props.navigation.state.params);

    navigate("Exercises", { user_id:user_id, workoutID: workoutID, workout_day: workout_day, workoutTitle: workoutTitle, courseID: courseID, courseTitle: courseTitle, course: course });
    
    }


    _getSensorData(){
      var { exerciseID, user_id, workoutID, workout_day, courseID  } = this.props.navigation.state.params;
      getSensorData(user_id, exerciseID, workoutID, workout_day, courseID).then(res=>{/*user_id, exercise_id, workout_id, workout_day, course_id*/
        console.log(res);
        if(res.data){
          console.log(res.data)
          this.setState({ timespent: res.data.time_interval })
          this.setState({ averageheartrate: res.data.avg_heart_rate })
          this.setState({ calories: res.data.calories })          
        }else{
          console.log('no sensor data')
          this.setState({ timespent: 0 })
          this.setState({ averageheartrate: 0 })
          this.setState({ calories: 0 })
        }
      },
      err=>{
        console.log(err);
      })
    }
    _sendStatus(statusvalue){
    var { exerciseID, user_id, workoutID, courseID, status, workout_day  } = this.props.navigation.state.params;
      let temp={
        "user_id":user_id,
        "exercise_id" : exerciseID,
        "workout_id" :workoutID,
        "course_id":courseID,
        "workout_day":workout_day,
        "exercise_status" : statusvalue
      };
      sendStatus(temp).then(res=>{
        console.log(res);
       if(statusvalue == 'completed')
        {
         const { navigate  } = this.props.navigation;
         navigate("Courses");
        }      
      },
      err=>{
        console.log(err);
      })
    }
     _getSensor(exercise_id){
    getSensorDataSQLite(exercise_id)
    .then(response=>{
      console.log("response", response);
      this.setState({sensor_data:response.data})
    }, reject=>{
      console.log("reject", reject);
      
    })
    .catch(err=>{
      console.log("error", err);
    })
  }
  render() {
    const flexCompleted = this.getCurrentTimePercentage() * 100;
    const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;
    let source = this.state.paused === true ? require( '../../Images/play.png' ) : require( '../../Images/pause.png' );
    var { status, exerciseID  } = this.props.navigation.state.params;

    setTimeout(() => {this.setState({timePassed: true})}, 2000)

      return (
      <View style={{flex: 1, height: 1200}}>

            <View style={styles.header}>

              <View style={styles.menupartleft}>

                  <TouchableOpacity onPress={() => this._openMenu() } >
                    <Text>
                    <Image style={styles.avatar} source={require('./../../Images/1280_back_arrow.png')}/>
                    </Text>
                  </TouchableOpacity>

              </View>
              <View style={styles.menupartright}>

              <Text style={styles.title}>{this.props.navigation.state.params.exeTitle}</Text>

              </View>

            </View>      
    
          <ScrollView automaticallyAdjustContentInsets={false}>
            <View style={{

            }}>

            <View style={styles.firstlock}> 
                <View style={styles.avgrateblock}>

                  <View style={styles.heartrate2}>
                  <Text style={styles.heartrate2text}>Duration</Text>
                  </View>
                  <View style={styles.heartrate4}>
                  <Text style={styles.heartrate3text}>{this.state.exercise.duration} Min.</Text>
                  </View>

                </View>   


                <View style={styles.avgrateblock}>

                  <View style={styles.heartrate2}>
                  <Text style={styles.heartrate2text}>Average Heart Beat</Text>
                  </View>
                  <View style={styles.heartrate4}>
                  <Text style={styles.heartrate3text}>{(status =='completed') || (status =='In progress')?this.state.averageheartrate:'0'}</Text>
                  </View>

                </View> 
                <View style={styles.avgrateblock}>

                  <View style={styles.heartrate2}>
                  <Text style={styles.heartrate2text}>Calories Burnt</Text>
                  </View>
                  <View style={styles.heartrate3}>
                  <Text style={styles.heartrate3text}>{(status =='completed') || (status =='In progress')?this.state.calories:'0'}</Text>
                  </View>

                </View>   
                <View style={styles.avgrateblock}>

                  <View style={styles.heartrate2}>
                  <Text style={styles.heartrate2text}>Status</Text>
                  </View>
                  <View style={styles.heartrate4}>
                  <Text style={styles.heartrate3text}>

                  { status == 0 ? ' Not Yet Started ' : status === 'completed' ? ' Completed ' : ' In Progress ' }


                  </Text>
                  { this.state.showSensorData ? 
                    <Button onPress={() => this._getSensor(exerciseID)}>graph data
                    </Button> 
                    :null 
                  }

                  
                  
                  </View>

                </View> 



            </View>


                   


            {this.state.exercise.mp4url?
            <View style={styles.video}>

              <TouchableOpacity onPress={() => this._playpause(this.state.play)}>
                <Video
                  ref={(ref: Video) => { this.video = ref }}
                  source={{ uri: this.state.exercise.mp4url }}/*this.state.exercise.streaming_uri*/
                  style={styles.fullScreen}
                  rate={this.state.rate}
                  paused={this.state.paused}
                  volume={this.state.volume}
                  muted={this.state.muted}
                  resizeMode = "stretch"
                  onLoad={this.onLoad}
                  onProgress={this.onProgress}
                  onEnd={this.onEnd}
                  onAudioBecomingNoisy={this.onAudioBecomingNoisy}
                  onAudioFocusChanged={this.onAudioFocusChanged}
                  repeat={false}
                />
              </TouchableOpacity>

                <View style={styles.controls}>
                  <View style={styles.trackingControls}>
                    <View style={styles.progress}>
                      <View style={[styles.innerProgressCompleted, { flex: flexCompleted }]} />
                      <View style={[styles.innerProgressRemaining, { flex: flexRemaining }]} />
                  </View>
                  <View style={styles.botttomcontrols}>
                    <TouchableHighlight style={styles.playpause} onPress={() => this._playpause(this.state.play)}>
                      <Image style={styles.playpauseimage}  source={ source } />
                    </TouchableHighlight>
                    <View style={styles.duration}>
                      <Text style={styles.controlercolor}>{Math.round(this.state.currentTime)} / {Math.round(this.state.duration)}</Text>
                    </View>
                  </View>

                  </View>
                </View>              

            </View>:<View style={styles.video}><Text style={{textAlign:'center',marginTop: 20,color: 'white'}}>No Video Avaliable</Text></View>}

            

                    {status =='completed'?<View></View>:<View style={styles.thirdblock}>
                      <View style={{marginTop:20}}>
                        <Button
                          buttonStyle={{marginTop:20}}
                          title="Start Exercise"
                          fontSize={20}
                          disabled={this.state.loader}
                          disabledStyle={{backgroundColor:'#ef7e2d'}}
                          loading={this.state.loader}
                          backgroundColor="#ef7e2d"
                          onPress={() => this._viewdevices() }
                        />

      
                      </View>
                    </View>}


              </View>
           </ScrollView> 
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
   justifyContent: 'center',
  },
  scroll: {
    flex: 1,
    height: 400,
  },
  firsttext: {
    fontSize: 20
  },
  innertext: {
    marginTop: 0,
    width: '50%'
  },
  body: {
    flex : 1,
    justifyContent: 'center'
  },
  fullScreen: {
    width: '100%',
    height: '100%',
  },
  tabcontent: {
    margin: 8,
    flex: 1,
    flexDirection: 'row', 
    width: '100%',
    alignSelf: 'center'  
  },
  leftcontent: {
    width: '50%',
    backgroundColor: 'red'
  },
  rightcontent: {
    width: '46%',
    backgroundColor: 'yellow'
  },
  avgrateblock: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
    margin: 10
  },
  heartrate2: {
    width: '50%',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  heartrate3: {
    width: '50%',
    alignSelf: 'flex-end',
  },
  videoimage: {
    alignSelf: 'stretch',
    height: 250,   
    borderBottomWidth: 2,
  },
  heartrate4: {
    width: '50%',
    alignSelf: 'flex-end',
    borderColor: '#000',
    backgroundColor: 'lightyellow',
    borderBottomWidth: 2    
  },  
  heartrate2text: {
    textAlign: 'left',
    fontSize: 20,
    color: '#000',
    marginLeft: 10
  },
  heartrate3text: {
    textAlign: 'right',
    fontSize: 24,
    color: '#ef7e2d',
    marginRight: 10
  },
  heartrate4text: {
    textAlign: 'right',
    fontSize: 30,
    color: '#ef7e2d',
    marginRight: 10
  },  
  video: {
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: 250,
    width: '100%',
    backgroundColor: '#000',
    marginTop: 20
  },
  thirdblock: {
    flex: 1,
    height: 130
  },
  firstlock: {
  },  
  list: {
    margin: 1,
    flex: 1,
  },
  lists: {
    flex: 1, 
    flexDirection: 'row',
    borderColor: '#000',
    borderBottomWidth: 1,
  },
  innerlist: {
    margin: 8,
    marginLeft: 10,
  },
  lowertext: {
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    margin: 4
  },
  length: {
    width: '65%',
    height: 10,
    alignItems: 'flex-start',
    
  },
  leftitem: {
    fontSize: 22,
    color: '#000',    
    textAlign: 'left',
    width: '67%'
  },
  rightitem: {
    fontSize: 16,
    paddingVertical: 5,
    color: '#000',
    marginLeft: 10,
    textAlign: 'left',
    width: '34%',
    alignSelf: 'flex-end'
  },   
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  header: {
    height: 60,
    backgroundColor: '#262626',
    width: '100%',
    flexDirection: 'row',
    shadowOffset: { width: 10, height: 10 },
    shadowColor: '#000',
    shadowOpacity: 1,
    elevation: 3
  },
  progressContainer: {
    alignItems: "flex-end", 
    width: '48%',
    height: 10,
    marginTop: 5,
  },  
  avatar: {
    height: 40,
    width: 60,
    marginLeft: 35,
    marginTop: 10
  },
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: -10

  },
  menupartleft: {
    width: '13%',
    marginLeft: 15,
    marginTop: 20
  },
  menupartright: {
    width: '87%',
    marginTop: 24,
  },
  textcenter: {
    justifyContent: 'center',
    textAlign: 'center'
  },
  controls: {
    backgroundColor: "transparent",
    borderRadius: 5,
    position: 'absolute',
    bottom: 10,
    left: 4,
    right: 4,
    marginTop: 15
  },
  progress: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 3,
    overflow: 'hidden',
  },
  controlercolor: {
    color: '#fff',
    fontSize: 20
  },
  innerProgressCompleted: {
    height: 5,
    backgroundColor: '#cccccc',
  },
  innerProgressRemaining: {
    height: 5,
    backgroundColor: '#2C2C2C',
  },
  generalControls: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 4,
    overflow: 'hidden',
    paddingBottom: 10,
  },
  rateControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  volumeControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  resizeModeControl: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  controlOption: {
    alignSelf: 'center',
    fontSize: 11,
    color: 'white',
    paddingLeft: 2,
    paddingRight: 2,
    lineHeight: 12,
  },
  playpause: {
    flex: 1,
  },
  duration: {
    justifyContent: 'flex-end',
  },
  botttomcontrols: {
    marginTop:10,
    flex: 1,
    flexDirection: 'row',
    width: '100%'
  },
  playpauseimage: {
    width: 20,
    height: 20
  }  
})

