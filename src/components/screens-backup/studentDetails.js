
import { View, Button, StyleSheet, Text } from 'react-native';
import React, { Component } from 'react';
import 
  { 
  addToTable, 
  showAllTable, 
  getCoursesOffline,
  getWorkoutsOffline,
  getExercisesOffline,  
  getSensorData, 
  getExercisesMetrics, 
  getExerciseSummary } from '../template/SQLiteOperationsOffline.js'
import { getCourses, getWorkouts, getExercises, signin } from '../template/api.js'

export default class StudentDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {
      AllCourses: [],
      AllCourses:"no data"
    };
  }

  componentWillMount(){
    console.log(this.props.navigation);
  }
  _getCourses=()=>{
    console.log("All courses under user id:1")
    getCourses().then(response=>{
      console.log(response)
    },err=>{
      console.log(err)
    })
  }
  _getExercises=()=>{
    console.log("All exercise under workout id :60")
    getExercises(60).then(response=>{
      console.log(response)
    },err=>{
      console.log(err)
    })
  }
  _getWorkouts=()=>{
    console.log("All workouts under course id :10")
    getWorkouts(10).then(response=>{
      console.log(response)
    },err=>{
      console.log(err)
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._getCourses}
            title="show course"
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._getWorkouts}
            title="show workout"
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._getExercises}
            title="show exercise"
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={()=>getSensorData()}
            title="show sensor data"
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={()=>getExerciseSummary()}
            title="show exercise summary"
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={()=>getExercisesMetrics()}
            title="show exercise metrics"
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
   flex: 1,
   justifyContent: 'center',
  },
  buttonContainer: {
    margin: 20
  },
  alternativeLayoutButtonContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  contentContainer: { paddingVertical: 20 }
})