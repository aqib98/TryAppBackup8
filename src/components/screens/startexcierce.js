import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet,ScrollView, Text, View, TouchableHighlight, Animated ,TouchableOpacity, Image, ToastAndroid } from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn, isSignedIn } from '../../config/auth';
import { signin, getCourses } from '../template/api.js'
import { AsyncStorage } from "react-native";

import LinearGradient from 'react-native-linear-gradient';

export default class StartExcierce extends Component {


  _video(){

    const { navigate  } = this.props.navigation;
    navigate("VideoExcierce");

  }

  _track(){

    const { navigate  } = this.props.navigation;
    navigate("TrackExcierce");

  }
  _log(){

    const { navigate  } = this.props.navigation;
    navigate("LogActivity");

  }



    render() {


      return (

          <View style={styles.container}>

                <TouchableHighlight onPress={() => this._video() }>
                  <View style={[styles.row, {backgroundColor: 'red'}]}>
                    <Text style={{fontSize: 12, color: 'white', padding: 10}}>Check Video</Text>
                  </View>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => this._track() }>
                  <View style={[styles.row, {backgroundColor: 'blue'}]}>
                    <Text style={{fontSize: 12, color: 'white', padding: 10}}>Track Excierce</Text>
                  </View>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => this._log() }>
                  <View style={[styles.row, {backgroundColor: 'green'}]}>
                    <Text style={{fontSize: 12, color: 'white', padding: 10}}>Log Activity</Text>
                  </View>
                </TouchableHighlight>                
                <TouchableHighlight onPress={() => this._close() }>
                  <View style={[styles.row, {backgroundColor: 'black'}]}>
                    <Text style={{fontSize: 12, color: 'white', padding: 10}}>Close</Text>
                  </View>
                </TouchableHighlight>                

          </View>  

        );
    }
}

var styles = StyleSheet.create({




});
