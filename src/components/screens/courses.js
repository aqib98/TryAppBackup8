import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet,ScrollView, Text, View, TouchableHighlight, Animated ,TouchableOpacity, Image, ToastAndroid } from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn, isSignedIn } from '../../config/auth';
import { signin, getCourses } from '../template/api.js'
import { AsyncStorage } from "react-native";


class ProgressBar extends Component {
  
  componentWillMount() {
    this.animation = new Animated.Value(this.props.progress);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.progress !== this.props.progress) {
      Animated.timing(this.animation, {
        toValue: this.props.progress,
        duration: this.props.duration
      }).start();
    }
  }
  
  
  render() {
    const {
      height,
      borderColor,
      borderWidth,
      borderRadius,
      barColor,
      fillColor,
      row
    } = this.props;

    const widthInterpolated = this.animation.interpolate({
      inputRange: [0, 1],
      outputRange: ["0%", "100%"],
      extrapolate: "clamp"
    })

    return (
      <View style={[{flexDirection: "row", height }, row ? { flex: 1} : undefined ]}>
        <View style={{ flex: 1, borderColor, borderWidth, borderRadius}}>
          <View
            style={[StyleSheet.absoluteFill, { backgroundColor: fillColor }]}
          />
          <Animated.View
            style={{
              position: "absolute",
              left: 0,
              top: 0,
              bottom: 0,
              width: widthInterpolated,
              backgroundColor: barColor
            }}
          />
        </View>
      </View>
    )
  }
}  

ProgressBar.defaultProps = {
  height: 6,
  borderWidth: 0,
  borderRadius: 0,
  barColor: "#3bafda",
  fillColor: "#d8d8d8",
  duration: 70
}


export default class Courses extends Component {

  constructor(props) {
      super(props);
      this.state={
        courses:[],
        userid:'',
        progress: 0.8,
        timePassed: false,
        loader:null
      }
  }



  componentWillMount(){
    const { navigate  } = this.props.navigation;
    this.setState({loader:true})
    this.getStudentID();
    
  }
  async getStudentID () {
  console.log("ff") 
    let USER_ID = await AsyncStorage.getItem("USER_ID"); 
    this.setState({userid:USER_ID},()=>{
      this._getAllCourse(this.state.userid);
    });
  }
  _getAllCourse=(id, )=>{
    this.setState({loader:'0'})
    getCourses(id).then(response=>{
       this.setState({loader:'1'})
      console.log(response.status)
      if(response.status){

          this.setState({courses:response.data},()=>{

          console.log(this.state.courses)
          for(let j=0;j<this.state.courses.length;j++)
          {

            if(j%2==0)
            {
              this.state.courses[j].color = '#FFF';
            }
            else
            {
              this.state.courses[j].color = '#f8f8f8';
            }

          }

          console.log(this.state.courses.length)

          if(this.state.courses.length == 0) { 

            this.setState({responslength : 0})  

            console.log('Manikanta')  

          }



          this.setState(this.state.courses);
          this.setState({loader:false})
        });
      }else{
        ToastAndroid.show(response.show, ToastAndroid.SHORT);

        console.log('Manikanta')

        this.setState({responslength : 0})
      }
    },err=>{
      console.log(err)

      console.log('Manikanta')

      this.setState({responslength : 0})

    })
  }


  openMenu = async() => {

    const { navigate  } = this.props.navigation;
    navigate("Menu");

  }

  _viewWorkouts = (id, title, course) => {
      const { navigate  } = this.props.navigation;
      console.log(course)
      navigate("Workouts", { courseID: id, courseTitle: title, user_id:this.state.userid, course:course });
  }

    render() {


      return (

          <View style={styles.container}>


            <View style={styles.header}>

              <View style={styles.menupartleft}>

                  <TouchableOpacity onPress={() => this.openMenu() } >
                    <Text>
                    <Image style={styles.avatar} source={require('./../../Images/640_menu_icon.png')}/>
                    </Text>
                  </TouchableOpacity>

              </View>
              <View style={styles.menupartright}>

              <Text style={styles.title}>Courses</Text>

              </View>

            </View>

            <View style={styles.body}>


            <ScrollView style={styles.scroll}>


            

            {this.state.loader == '1'?<Image style={{flex:1,alignSelf:'center',width: 100, height: 100,marginTop: 50}} source={require('./../../Images/loading.gif')} />:this.state.courses.length?<FlatList style={styles.list} data={this.state.courses}
            KeyExtractor={(x, i) => i}
            renderItem={({ item, j }) =>
            <View style={styles.lists} style={{backgroundColor:item.color,flex: 1,flexDirection: 'row',borderColor: '#f5f4f4',borderBottomWidth:1,borderTopWidth: 1,shadowOffset: { width: 100, height: 100 },shadowColor: '#000',shadowOpacity: 10,shadowRadius: 10}}>

            <View style={styles.innerlist} style={{margin: 12,marginLeft: 10}}>            
              
              <TouchableOpacity  onPress={() => this._viewWorkouts(item.course_id, item.title, item) }>

              <Text style={styles.firsttext}> {item.title.length<20?item.title:item.title.substring(0,20)+'...'}   </Text>

              <View style={styles.lowertext}>

                <View style={styles.innertext}> 

                  <Text>({item.course_days} days)  | { item.course_status == '0' ? ' Not Yet Started ' : item.course_status == '100' ? ' Completed ' : ' In Progress ' }

                  

                </Text> 

                </View>

                <View style={styles.progressContainer}>

                  <View style={styles.length}>

                    <ProgressBar
                      row
                      progress={item.course_status/100}
                    />


                    <Text style={{alignSelf: 'flex-end'}}>{ item.course_status == '100'? '100': item.course_status}%</Text>  
                  </View>  
                </View>     

               </View>   

               </TouchableOpacity> 

             </View> 
                        
            </View>
            }
            />:<View style={{flex: 1,margin: 10, marginTop: 30}}><Text style={{fontSize: 28, textAlign: 'center'}}>No Courses Under This Student or check your internet connection</Text></View>}

            </ScrollView>

            </View>

          </View>  

        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   justifyContent: 'center',
  },
  scroll: {
    flex: 1
  },
  firsttext: {
    fontSize: 20,
    fontWeight:'bold'
  },
  innertext: {
    marginTop: 0,
    width: '50%'
  },
  body: {
    flex : 1,
    justifyContent: 'center'
  },
  list: {
    margin: 1,
    flex: 1,   
  },
  lists: {
    flex: 1, 
    flexDirection: 'row',
    borderColor: 'red',
    borderBottomWidth: 2,
    shadowOffset: { width: 100, height: 100 },
    shadowColor: '#000',
    shadowOpacity: 10,
    shadowRadius: 10
  },
  lowertext: {
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    margin: 4
  },
  length: {
    width: '65%',
    alignItems: 'flex-start',
    
  },
  leftitem: {
    fontSize: 22,
    color: '#000',    
    textAlign: 'left',
    width: '67%'
  },
  rightitem: {
    fontSize: 16,
    paddingVertical: 5,
    color: '#000',
    marginLeft: 10,
    textAlign: 'left',
    width: '34%',
    alignSelf: 'flex-end'
  },   
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  header: {
    height: 60,
    backgroundColor: '#262626',
    width: '100%',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOpacity: 0.2 
  },
  progressContainer: {
    alignItems: "flex-end", 
    width: '48%',
    marginTop: -5,
  },  
  avatar: {
    height: 40,
    width: 60,
    marginLeft: 35,
    marginTop: 10
  },
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: -10,
  },
  menupartleft: {
    width: '15%',
    marginLeft: 15,
    marginTop: 20
  },
  menupartright: {
    width: '85%',
    marginTop: 24,
  },
  textcenter: {
    justifyContent: 'center',
    textAlign: 'center'
  }
})

module.exports = Courses;