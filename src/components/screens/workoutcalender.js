import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet,ScrollView, Text, View, TouchableHighlight, Animated ,TouchableOpacity, Image, ToastAndroid } from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn, isSignedIn } from '../../config/auth';
import { signin, getCourses } from '../template/api.js'
import { AsyncStorage } from "react-native";


import TimerCountdown from 'react-native-timer-countdown';

export default class TrackExcierce extends Component {
  render() {
      return (
          <TimerCountdown
              initialSecondsRemaining={360}
              onTick={() => console.log('tick')}
              onTimeElapsed={() => console.log('complete')}
              allowFontScaling={true}
              style={{ fontSize: 20 }}
          />
      )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   justifyContent: 'center',
  }
})

