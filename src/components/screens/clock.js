import React from "react";
import { View } from "react-native";
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn } from '../../config/auth';
import API from '../template/constants.js';
export default class Clock extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      Password: "",
      confirmPassword: "",
      name: "",
      username: "",
      showErrorEmail: false,
      showErrorPassword: false,
      showErrorConfirmPassword: false,
      showErrorName: false,
      showErrorUsername: false,
      errorEmailMessage: "",
      errorPasswordMessage: "",
      errorConfirmPasswordMessage: "",
      errorUsername: "",
      errorName: ""

    };
  }
  componentWillMount(){
    console.log(this.props.navigation);
  }




/*authenticating starts*/
  _authenticateSignup = async () => {
    const { navigate } = this.props.navigation;

    onSignIn().then(() => navigate("SignedIn"));

    /*

	if(this.state.name===""){


      this.setState(
        {
          showErrorEmail:false,
          showErrorPassword:false,
          showErrorConfirmPassword:false,
          showErrorName:true,
          showErrorUserName:false,
          errorName:"check Name",           
        }
      )		

	}
	else if(this.state.username==="") {

      this.setState(
        {
          showErrorEmail:false,
          showErrorPassword:false,
          showErrorConfirmPassword:false,
          showErrorName:false,
          showErrorUserName:true,
          errorUserName:"check Email",           
        }
      )	

	}
	else if(this.state.email==="") {

      this.setState(
        {
          showErrorEmail:true,
          showErrorPassword:false,
          showErrorConfirmPassword:false,
          showErrorName:false,
          showErrorUserName:false,
          errorEmailMessage:"check Email",          
        }
      )

	}
	else if(this.state.password===""){

      this.setState(
        {
          showErrorEmail:false,
          showErrorPassword:true,
          showErrorConfirmPassword:false,
          showErrorName:false,
          showErrorUserName:false,
          errorPasswordMessage:"check Password",           
        }
      )

	}
	else if(this.state.confirmPassword===""){

      this.setState(
        {
          showErrorEmail:false,
          showErrorPassword:false,
          showErrorConfirmPassword:true,
          showErrorName:false,
          showErrorUserName:false,                
          errorConfirmPasswordMessage:"check Confirm Password"          
        }
      )	

	} else {

      if(this.state.password===this.state.confirmPassword)
      {
        try 
        {
          const response = await fetch(API.Signup,{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "name":  this.state.name,
                "username": this.state.username,
                "email":  this.state.email,
                "type":  'S',
                "password":  this.state.password
            })
          })
          const auth = await response.json()
          console.log(auth)
          if(auth.MessageResponse){
            onSignIn().then(() => navigate("SignedIn"));
          }else
          {
            this.setState(
              {
                showErrorEmail:false,
                showErrorPassword:false,
                showErrorConfirmPassword:false,
			    showName: false,
			    showUsername: false,                
                errorEmailMessage:"check Email",
                errorPasswordMessage:"check Password",
                errorConfirmPasswordMessage:"check Confirm Password"
              }
            )
          }
        } catch (e) 
        {
          console.log(e);
        }
      }else
      {
        this.setState(
          {
            showErrorConfirmPassword:true,  
            errorConfirmPasswordMessage:"Password not match" 
          }
        )
      }
    }

    */

  }
/*authenticating starts*/


  render(){
    const { navigate } = this.props.navigation;
    return(
      <View style={{ paddingVertical: 20 }}>
        <Card>

          <FormLabel>FirstName</FormLabel>
          <FormInput 
            placeholder="Full Name..." 
            onChange={ (event) => this.setState({name:event.nativeEvent.text}) }
          />
          {this.state.showErrorName?
            <FormValidationMessage>
              {this.state.errorName}
            </FormValidationMessage>
            :
            null
          }
          <FormLabel>Username</FormLabel>
          <FormInput 
            placeholder="UserName..." 
            onChange={ (event) => this.setState({username:event.nativeEvent.text}) }
          />
          {this.state.showErrorUsername?
            <FormValidationMessage>
              {this.state.errorUsername}
            </FormValidationMessage>
            :
            null
          }
          <FormLabel>Email</FormLabel>
          <FormInput 
            placeholder="Email address..." 
            onChange={ (event) => this.setState({email:event.nativeEvent.text}) }
          />
          {this.state.showErrorEmail?
            <FormValidationMessage>
              {this.state.errorEmailMessage}
            </FormValidationMessage>
            :
            null
          }
          <FormLabel>Password</FormLabel>
          <FormInput secureTextEntry 
            placeholder="Password..."
            onChange={ (event) => this.setState({password:event.nativeEvent.text}) }
          />
          {this.state.showErrorPassword?
            <FormValidationMessage>
              {this.state.errorPasswordMessage}
            </FormValidationMessage>
            :
            null
          }
          <FormLabel>Confirm Password</FormLabel>
          <FormInput secureTextEntry 
            placeholder="Confirm Password..."
            onChange={ (event) => this.setState({confirmPassword:event.nativeEvent.text}) }
          />
          {this.state.showErrorConfirmPassword?
            <FormValidationMessage>
              {this.state.errorConfirmPasswordMessage}
            </FormValidationMessage>
            :
            null
          }
          <Button
            buttonStyle={{ marginTop: 20 }}
            backgroundColor="#03A9F4"
            title="SIGN UP"
            onPress={ () => this._authenticateSignup() }
          />
        </Card>
      </View>
    );
  }
}
