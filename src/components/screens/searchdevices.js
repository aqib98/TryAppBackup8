/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  ListView,
  ScrollView,
  AppState,
  TouchableOpacity, Image, ToastAndroid
} from 'react-native';
import Dimensions from 'Dimensions';
import BleManager from 'react-native-ble-manager';
import TimerMixin from 'react-timer-mixin';
import reactMixin from 'react-mixin';
import moment from "moment";


import { addToSQLiteTable } from '../template/SQLiteOperationsOffline.js'; 
import { addAsyncStorage } from '../../config/auth.js'

const window = Dimensions.get('window');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);


import PTRView from 'react-native-pull-to-refresh'

export default class SearchDevices extends Component {


  constructor(){
    super()

    this.state = {
      scanning:false,
      peripherals: new Map(),
      appState: '',
      heartrate: 0,
      screenchange: '0',
      time: moment().format("LTS"),
      date: moment().format("LL"),
      totsec: 2400,
      item: 0,
      array: [],
      sum: 0,
      average: 0,
      calories: 0,
      user_id:null,
      profile:[],
      height:'',
      weight:'',
      timepassed: false,
      rate: 1,
      volume: 1,
      duration: 0.0,
      currentTime: 0.0,
      paused: true,
      loader:false,
      times:0
    }

    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);
    this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(this);
    this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(this);
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
  }

  componentDidMount() {


    AppState.addEventListener('change', this.handleAppStateChange);

    BleManager.start({showAlert: false});

    this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );
    this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan );
    this.handlerDisconnect = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral );
    this.handlerUpdate = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic );



    if (Platform.OS === 'android' && Platform.Version >= 23) {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
            if (result) {
              console.log("Permission is OK");
            } else {
              PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                  console.log("User accept");
                } else {
                  console.log("User refuse");
                }
              });
            }
      });
    }


  }

  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
      BleManager.getConnectedPeripherals([]).then((peripheralsArray) => {
        console.log('Connected peripherals: ' + peripheralsArray.length);
      });
    }
    this.setState({appState: nextAppState});
  }

  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
    this.handlerDisconnect.remove();
    this.handlerUpdate.remove();
  }

  handleDisconnectedPeripheral(data) {
    let peripherals = this.state.peripherals;
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      this.setState({peripherals});
    }
    console.log('Disconnected from ' + data.peripheral);
  }

  handleUpdateValueForCharacteristic(data) {
    //console.log('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
    this.setState({heartrate: data.value[1]});

    //var { calories } = this.props.navigation.state.params;

    this.savedata(data.value[1])

  }

  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({ scanning: false });
  }

  startScan() {
    if (!this.state.scanning) {
      BleManager.scan([], 30, true).then((results) => {
        console.log('Scanning...');
        ToastAndroid.show('Scanning....' , ToastAndroid.SHORT);
        this.setState({scanning:true});
      });
    }
  }

  handleDiscoverPeripheral(peripheral){
    var peripherals = this.state.peripherals;
    if (!peripherals.has(peripheral.id)){
      console.log('Got ble peripheral', peripheral);


      peripherals.set(peripheral.id, peripheral);

      console.log(peripherals)

      this.setState({ peripherals })
    }
  }

  _refresh () {
    
    return new Promise((resolve) => {

      setTimeout(()=>{        
        resolve(); 
      }, 2000)


      BleManager.scan([], 3, true).then((results) => {
        console.log('Scanning...');
        ToastAndroid.show('Scanning....' , ToastAndroid.SHORT);
        this.setState({scanning:true});
      });

    })
  }

  test(peripheral) {

    /*
        
    if (peripheral){
      if (peripheral.connected){
        BleManager.disconnect(peripheral.id);
      }else{
        BleManager.connect(peripheral.id).then(() => {
          let peripherals = this.state.peripherals;
          let p = peripherals.get(peripheral.id);
          if (p) {
            p.connected = true;
            peripherals.set(peripheral.id, p);
            this.setState({peripherals});
          }

          ToastAndroid.show('Connected' , ToastAndroid.SHORT);


          
        }).catch((error) => {
          console.log('Connection error', error);
          ToastAndroid.show('Connection error.... Please Try To Reconnect' , ToastAndroid.SHORT);
        });
      }
    }

    */

    addAsyncStorage({"device_id":"E5:EE:12:90:02:8A", "device_name":"HRM DEVICES"})
      .then(res=>{
        console.log(res)
      },err=>{
        console.log(err)
    })


  }
  render() {

    const list = Array.from(this.state.peripherals.values());
    const dataSource = ds.cloneWithRows(list);

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>PullToRefreshView Demo</Text>
        </View>


        <PTRView
          style={{backgroundColor:'#404040'}}
          onRefresh={this._refresh}
        >
          

        <ScrollView style={styles.scroll}>
          {(list.length == 0) &&
            <View style={{flex:1, margin: 20}}>
              <Text style={{textAlign: 'center',color: 'white'}}>No Devices</Text>
            </View>
          }
          <ListView
            enableEmptySections={true}
            dataSource={dataSource}
            renderRow={(item) => {
              const color = item.connected ? 'green' : '#333333';
              return (
                <TouchableHighlight onPress={() => this.test(item) }>
                  <View style={[styles.row, {backgroundColor: color}]}>
                    <Text style={{fontSize: 12, color: 'white', padding: 10}}>{item.name}</Text>
                  </View>
                </TouchableHighlight>
              );
            }}
          />

        </ScrollView>



        </PTRView>
        <View style={styles.loginButton}>
           <TouchableHighlight>
              <Text style={{textAlign:'center'}}>Save Devices</Text>
           </TouchableHighlight>          
        </View>        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#000',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  header: {
    height: 60,
    borderColor: '#fff',
    backgroundColor: '#000000',
    borderBottomWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    color: '#fff',
    fontSize: 20,
    lineHeight: 40,
  },
  card: {
    flex: 1,
    borderColor: '#fafafa',
    backgroundColor: '#2196F3',
    borderWidth: 2,
    borderRadius: 3,
    margin: 5,
  },
  card__text: {
    color: '#fafafa',
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  loginButton: {
     position:'absolute',
     bottom: 0,
     height:60,
     backgroundColor:'#fff',
     justifyContent:'center',
     width: '100%'
   }  
});

