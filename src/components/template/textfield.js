import React, { Component } from 'react';
import { Text, StyleSheet, View, TouchableOpacity, TextInput } from 'react-native';
	
const CustomTextField = ({ children, onChangeText, placeholder}) => {
	const { textFieldStyle } = styles;
	return (
		<View>

			<TextInput style = {textFieldStyle}
               underlineColorAndroid = 'rgba(0,0,0,0)'
               placeholder = {placeholder}
               placeholderTextColor = "#E3E6E3"
               autoCapitalize = "none"
               onChangeText = {onChangeText(e)}
            />
		</View>
	);
};

const styles = StyleSheet.create({
	textFieldStyle: {
		paddingTop: 5,
		paddingBottom: 5,
		paddingLeft: 10,
		paddingRight: 10
	}
}); 

export default CustomTextField;
