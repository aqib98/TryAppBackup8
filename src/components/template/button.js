import React, { Component } from 'react';
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native';
	
const Button = ({onPress, children}) => {
	const { buttonStyle, buttonTextStyle } = styles;
	return (
		<TouchableOpacity onPress={onPress} style={buttonStyle}>
			<Text style={buttonTextStyle}>
				{children}
			</Text>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	buttonStyle: {
		width: 120,
		backgroundColor: '#fff',
		borderRadius: 5,
		borderColor: '#429DC6',
		borderWidth: 2,
		marginTop: 2,
		marginBottom: 2,
		marginLeft: 5,
		marginRight: 5
	},
	buttonTextStyle: {
		alignSelf: 'center',
		color: '#429DC6',
		fontSize: 16,
		fontWeight: '500',
		paddingTop: 5,
		paddingBottom: 5
	}
}); 

export default Button;
