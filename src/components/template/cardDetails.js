import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
	
const CardDetails = (props) =>{
	return (
		<View style={styles.containerStyle}>
			{props.children}
		</View>
	);
};

const styles = StyleSheet.create({
	containerStyle: {
		borderBottomWidth: 1,
		padding: 5,
		backgroundColor: '#fff',
		justifyContent: 'flex-start',
		flexDirection: 'row',
		borderColor: '#fff',
	}
}); 

export default CardDetails;
